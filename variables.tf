variable "default_tags" {
    type = map
    default = {
        Owner: "soringuzun"
        Team: "Ops"
    }
}