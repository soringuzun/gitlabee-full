resource "aws_iam_role" "gitlabLambdaRole" {
  name = "gitlabLambdaRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  tags = var.default_tags
}


resource "aws_iam_role_policy_attachment" "attachEC2full" {
  role       = aws_iam_role.gitlabLambdaRole.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}

resource "aws_iam_role_policy_attachment" "attachCloudWatch" {
  role       = aws_iam_role.gitlabLambdaRole.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
}