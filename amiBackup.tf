resource "aws_lambda_function" "amiBackup" {
    function_name = "gitlab-amiBackup-24h"
    description = "Backup gitlab instance every 24h with retain period 3 days"
    filename = "amiBackup.zip"
    handler = "main.gitlabLambdaBackup"
    runtime = "python3.8"
    role = aws_iam_role.gitlabLambdaRole.arn
    tags = var.default_tags
}


resource "aws_cloudwatch_event_rule" "EveryDay" {
  name                = "start-gitlabAmiBackup-Lambda-everyDay"
  description         = "Create GitLabInstance backup every day with 3 days retention"
  schedule_expression = "cron(0 1 * * ? *)"
  tags = var.default_tags
}


resource "aws_cloudwatch_event_target" "checkEveryDay" {
  rule      = aws_cloudwatch_event_rule.EveryDay.name
  target_id = "lambda"
  arn       =  aws_lambda_function.amiBackup.arn

}


resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.amiBackup.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.EveryDay.arn
}

