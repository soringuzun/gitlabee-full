#commented blocks are for another scenario where gitlab is in a privatesubnet behind nat

provider "aws" {
  region = "us-east-1"
}

resource "aws_security_group" "gitlab" {
  name        = "gitlabSG"
  description = "Allow inbound traffic"

  ingress {
    description = "OpenSSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Web HTTP"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_groups = [data.aws_security_group.proxy.id]
  } 

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.default_tags
}

resource "aws_security_group" "k8s_soringuzun" {
  name        = "k8s_soringuzun"
  description = "Allow inbound traffic"

  ingress {
    description = "OpenSSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow internodes traffic"
    from_port = 0
    to_port = 0
    protocol = "-1"
    self = true
  } 

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.default_tags
}



/* 
resource "aws_security_group" "web" {
  name        = "webSG"
  description = "Allow inbound traffic for WebServer"

  ingress {
    description = "SSH ACCESS"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] 
  }

  ingress {
    description = "Web HTTPS"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  } 

  ingress {
    description = "Web HTTP"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  } 

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
 */

resource "aws_key_pair" "gitlabKey" {
  key_name   = "gitlabKey"
  public_key = file("keys/gitlabKey.pub")

  tags = var.default_tags
}

resource "aws_key_pair" "k8s" {
  key_name   = "k8s_soringuzun"
  public_key = file("keys/k8s.pub")

  tags = var.default_tags
}

resource "aws_instance" "gitlab" {
  ami           =  data.aws_ami.amazonLinux.id
  instance_type = "t3.medium"
  vpc_security_group_ids = [aws_security_group.gitlab.id]
  key_name = "gitlabKey"
  private_ip = "172.31.6.144"
  subnet_id = data.aws_subnet.internshipSubnet.id
  associate_public_ip_address = true
  availability_zone = "us-east-1a"
  user_data = data.template_file.gitlab.rendered #setup user for ansible
  root_block_device {
    volume_size = 30
    volume_type = "gp2"
  }
  #depends_on = [aws_nat_gateway.natGW]
  tags = merge(map("amiBackup","true", "Name", "Gitlab EE"), var.default_tags)
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.gitlab.id
  allocation_id = aws_eip.gitlab.id
}




/* 
resource "aws_instance" "web" {
  ami           =  data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.web.id]
  key_name = "gitlabKey"
  private_ip = "172.31.48.5"
  subnet_id = aws_subnet.internshipPublic.id
  availability_zone = "us-east-1a"
  associate_public_ip_address = true
  user_data = data.template_file.nginx.rendered

  tags = {
    "Name" = "test_web"
    "Owner" = "soringuzun"
    "Discipline" = "DevOps"
    "Purpose" = "Internship Final Project"
  }
}
 */

resource "aws_instance" "k8s_master" {
  ami           =  data.aws_ami.ubuntu.id
  instance_type = "t3.small"
  vpc_security_group_ids = [aws_security_group.k8s_soringuzun.id]
  key_name = "k8s"
  private_ip = "172.31.6.145"
  subnet_id = data.aws_subnet.internshipSubnet.id
  associate_public_ip_address = true
  availability_zone = "us-east-1a"
  #user_data = data.template_file.gitlab.rendered #setup user for ansible
  root_block_device {
    volume_size = 50
    volume_type = "gp2"
  }
  #depends_on = [aws_nat_gateway.natGW]
  tags = merge(map("Name", "k8s_master"), var.default_tags)
}

resource "aws_instance" "k8s_node1" {
  ami           =  data.aws_ami.ubuntu.id
  instance_type = "t3.small"
  vpc_security_group_ids = [aws_security_group.k8s_soringuzun.id]
  key_name = "k8s"
  private_ip = "172.31.6.146"
  subnet_id = data.aws_subnet.internshipSubnet.id
  associate_public_ip_address = false
  availability_zone = "us-east-1a"
  #user_data = data.template_file.gitlab.rendered #setup user for ansible
  root_block_device {
    volume_size = 50
    volume_type = "gp2"
  }
  #depends_on = [aws_nat_gateway.natGW]
  tags = merge(map("Name", "k8s_master"), var.default_tags)
}