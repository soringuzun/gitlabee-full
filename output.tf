output "GitLab_PrivateIP" {
  value = aws_instance.gitlab.private_ip
}

output "GitLab_PublicIP" {
  value = aws_eip.gitlab.public_ip
}

