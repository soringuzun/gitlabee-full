#Scenario when gitlabInstance is in privateSubnet behind nat and web server in publicSubnet

/* resource "aws_subnet" "internshipPublic" {
  vpc_id = data.aws_vpc.default.id
  cidr_block = "172.31.48.0/24"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = false
  
  tags = {
      Name = "devopsInternshipPublicSubnet"
  }
} */

/* 
resource "aws_subnet" "internshipPrivateSubnet" {
  vpc_id = data.aws_vpc.default.id
  cidr_block = "172.31.64.0/24"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = false
  
  tags = {
      Name = "devopsInternshipPrivateSubnet"
  }
}


resource "aws_route_table" "internshipPrivateRoute" {
  vpc_id = data.aws_vpc.default.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.natGW.id
  }

}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.internshipPrivateSubnet.id
  route_table_id = aws_route_table.internshipPrivateRoute.id
}

resource "aws_nat_gateway" "natGW" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.internshipPublic.id
} */


resource "aws_eip" "gitlab" {
  network_border_group = "us-east-1"
  tags = merge(map("gitlabEIP", "true"), var.default_tags)
  }