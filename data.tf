#Get Amazon Linux 2 ID - latest version 
data "aws_ami" "amazonLinux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

    owners = ["amazon"]

}


data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

    owners = ["099720109477"]

}

 
data "template_file" "gitlab" {
  template = file("templates/gitlab.yml")
}

data "template_file" "nginx" {
  template = file("templates/nginx.yml")
}

data "aws_vpc" "default" {
  default = true
}

data "aws_security_group" "proxy" {
  tags = {
    "Name" = "Internship2020"
  }
} 

data "aws_subnet" "internshipSubnet" {
  tags = {
    "Name" = "internship"
  }
}