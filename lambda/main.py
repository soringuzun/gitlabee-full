import boto3
import datetime
import re
import logging
import sys

ec2 = boto3.resource('ec2')
client = boto3.client('ec2')

today = datetime.date.today()

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# default retention period = 3 days
retentionDate = today - datetime.timedelta(days=3)

def deleteSnapshot(amiId):
    for i in client.describe_snapshots(OwnerIds=['self']).get('Snapshots'):
        snapId = i.get('SnapshotId')
        if amiId in i.get('Description'):
            client.delete_snapshot(SnapshotId=snapId)
           

def gitlabLambdaBackup(event, context):

    # Get imagesId and creation date
    images = client.describe_images(Owners=['self'])['Images']

    gitlabAMI = {}

    for i in range(len(images)):
        if images[i]['Name'].startswith('gitlabLamda-'):
            date = images[i]['CreationDate']
            match = re.search(r'\d{4}-\d{2}-\d{2}', date)
            fmtDate = datetime.datetime.strptime(
                match.group(), '%Y-%m-%d').date()
            gitlabAMI.update(
                {
                    images[i]['ImageId']: fmtDate
                }
            )

    if not gitlabAMI:
        logger.info("Warning: Images not found. Continuing........")


    # Check retention condition and delete old images
    for key, value in gitlabAMI.items():
        if value <= retentionDate:
            client.deregister_image(
                ImageId=key
            )    
            deleteSnapshot(amiId = key)
        elif value > retentionDate or value == today:
            logger.info(
                "- AMI for today already Created and no old AMI found to detele.")
            exit()
        else:
            logger.error(" - Something went wrong")
            exit()
    
        # Get instances by tag amiBackup = true
    instances = ec2.instances.filter(
        Filters=[{'Name': 'tag:amiBackup', 'Values': ['true']}, {
            'Name': 'instance-state-name', 'Values': ['running','stopped']}]
    )
    instanceIds = [s.id for s in instances]
    instanceCount = len(instanceIds)

    for i in instanceIds:
        if instanceCount == 0:
            logger.error("No running instances found.Skipping......")
        else:
            client.create_image(
                InstanceId=i,
                Name="gitlabLamda-" + str(today),
                NoReboot=True,
                DryRun=False
            )
        logger.info("Created AMI: gitlabLamda-" + str(today))
    
    return

